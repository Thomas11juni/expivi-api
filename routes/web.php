<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['middleware' => 'cors'], function () use ($app) {
    $app->get('/things', [
        'as' => 'get_things',
        'uses' => 'ThingController@index'
    ]);

    $app->get('/thing/{id}', [
        'as' => 'get_thing',
        'uses' => 'ThingController@show'
    ]);

    $app->post('/thing/{id}/update', [
        'as' => 'post_thing_update',
        'uses' => 'ThingController@update'
    ]);
});
