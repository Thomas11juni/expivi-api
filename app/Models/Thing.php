<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thing extends Model 
{
    protected $fillable = [
        'value_1', 'value_2', 'value_3', 'secret_value_1', 'secret_value_2'
    ];
}
