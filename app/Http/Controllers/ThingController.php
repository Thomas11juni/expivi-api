<?php

namespace App\Http\Controllers;

use App\Models\Thing;
use Illuminate\Http\Request;

class ThingController extends Controller
{

    public function index ()
    {
        $things = Thing::all();

        $data = [
            'things' => $things
        ];

        return response()->json($data);
    }

    public function show (Request $request, $id)
    {
        $thing = Thing::find($id);

        if (!$thing) abort(404);
        

        $data = [
            'thing' => $thing
        ];

        return response()->json($data);
    }

    public function update (Request $request, $id)
    {
        $thing = Thing::find($id);

        if (!$thing) abort(404);


        $thing->fill($request->all());

        $thing->save();

        return response()->json((object) ['status' => 'success']);
    }

}
