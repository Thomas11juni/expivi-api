<?php

use App\Models\Thing;
use Illuminate\Database\Seeder;

class ThingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amount = 10;

        for ($i = 0; $i < $amount; $i++)
        {
            Thing::create([
                'value_1' => str_random(6),
                'value_2' => str_random(6),
                'value_3' => str_random(6)
            ]);
        }
    }
}
